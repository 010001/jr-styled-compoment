import logo from './logo.svg';
import './App.css';
import styled from "styled-components";
import {StyledButton2} from "./compoments/styles/StyledButton2.styled";
import {StyledButton3} from "./compoments/styles/StyledButton3.styled";
import GlobalStyles from './GlobalStyles';

const StyledButton = styled.button`
  background-color:#646464;
  font-size: 32px;
  color: white;
`;

function App() {
  return (
    <div className="App">
      <GlobalStyles/>
      <StyledButton> Button 1 inline</StyledButton>
      <StyledButton2>button 2 external</StyledButton2>
      <StyledButton3 bg={"yellow"}>Button 3 Props</StyledButton3>
    </div>
  );
}

export default App;
